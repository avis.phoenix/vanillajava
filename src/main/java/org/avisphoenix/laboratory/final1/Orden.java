package org.avisphoenix.laboratory.final1;

import java.io.PrintStream;
import java.util.LinkedList;
import java.util.List;

public class Orden {
    private static int counter;
    public static final int MAX_COMPUTADORAS=10;

    private int id;
    private Computadora computadoras[];
    private int counterComputadoras;
    
    public Orden(){
        id=counter;
        counter++;

        computadoras = new Computadora[MAX_COMPUTADORAS];
        counterComputadoras=0;
    }

    public Orden(String json){
        id=counter;
        counter++;

        computadoras = new Computadora[MAX_COMPUTADORAS];
        counterComputadoras=0;
        loadJSON(json);
    }

    public void agregarComputadora(Computadora c) throws OutOfMemoryError{
        if (counterComputadoras < MAX_COMPUTADORAS){
            computadoras[counterComputadoras] = c;
            counterComputadoras++;
        }else throw new OutOfMemoryError();
    }

    public void mostrarOrden(){
        mostrarOrden(System.out);
    }

    public void mostrarOrden(PrintStream out){
        out.println("Orden No. " + String.format("%4d", id));
        out.println("|----------------------------------------|");
        out.println("Detalles:");
        for(int i=0; i < counterComputadoras; i++){
            out.println(computadoras[i].displayString(1));
        }
        out.println("|----------------------------------------|");
        out.println("Fin Orden: " +  String.format("%4d", id));
    }

    public String toStringJSON(){
        StringBuilder json = new StringBuilder();
        json.append("{");
        json.append("id: " + id);
        json.append(", computadoras: [");
        for(int i=0; i < counterComputadoras-1; i++){
            json.append(computadoras[i].toString());
            json.append(", ");
        }
        if (counterComputadoras > 0){
            json.append(computadoras[counterComputadoras-1].toString());
        }
        json.append("], type: \"Orden\"}");
        return  json.toString() ;
    }

    public void loadJSON(String json){
        String temp = json.split("computadoras:")[1];
        String computadorasStr = temp.split("]")[0];
        int begin = computadorasStr.indexOf("[");
        computadorasStr = computadorasStr.substring(begin+1);
        List<String> computadoras = splitObjects(computadorasStr);

        int end = Math.min(computadoras.size(), MAX_COMPUTADORAS);

        counterComputadoras = 0;
        for(int i=0; i < end; i++){
            try {
                this.computadoras[counterComputadoras] =  new Computadora(computadoras.get(i));
            } catch (Exception e){
                System.out.println("ERROR: al intentar interpretar una computadora. Mensaje: " + e.getMessage());
            }
            counterComputadoras++;
        }

    }

    private List<String> splitObjects(String line){
        int begin=0,end=0, level=0, i;
        List<String> objects = new LinkedList<>();
        for(i=0; i < line.length(); i++){
            if (line.charAt(i) == '{'){ level++; if (level == 1) begin=i; }
            else if (line.charAt(i) == '}') { level--; if (level == 0) end=i+1;}
            else if (line.charAt(i) == ',' && level == 0){
                objects.add(line.substring(begin, end));
            }
        }
        if (level==0){ objects.add(line.substring(begin, end));}

        return objects;
    }
}

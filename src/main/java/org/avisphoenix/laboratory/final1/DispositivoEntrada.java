package org.avisphoenix.laboratory.final1;

public class DispositivoEntrada {
    protected String marca, tipo;
    
    public static final String Marcas[] = { "Samsung", "Microsoft", "LG", "HP", "Asus", "Lenovo", "Apple" };
    public static final String DipositivosTipos[] = { "Alámbrico", "Inalámbrico" };

    public DispositivoEntrada(String marca, String tipo){ this.marca = marca; this.tipo = tipo;}
    
    @Override
    public String toString(){
        return "{ marca: \"" +  marca + "\", tipo: \"" + tipo + "\", type: \"DispositivoEntrada\" }";
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}

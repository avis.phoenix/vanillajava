package org.avisphoenix.laboratory.final1;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class Keyboard extends DispositivoEntrada {
    private int id;
    private static int counter;

    public Keyboard(String marca, String tipo){
        super(marca,tipo);
        id=counter;
        counter++;
    }

    public Keyboard(String json){
        super("","");
        id=counter;
        counter++;

        loadJSON(json);
    }

    @Override
    public String toString(){
        return "{ id: " + id + ", DispositivoEntrada: " + super.toString() + ", type: \"Keyboard\"}";
    }

    public void loadJSON(String json){
        JsonObject myJSON = JsonParser.parseString(json).getAsJsonObject();
        marca = myJSON.get("DispositivoEntrada").getAsJsonObject().get("marca").getAsString();
        tipo = myJSON.get("DispositivoEntrada").getAsJsonObject().get("tipo").getAsString();
    }

    public String displayString(int indent){
        String indentSpace="";
        for (int i=0; i < indent; i++) indentSpace+="\t";
        return "Teclado #" + String.format("%4d",id) +":\n" +
                indentSpace + "Marca: " + marca + "\n" +
                indentSpace + "Tipo: " + tipo;
    }

    public static int globalCounter() { return counter; }
}

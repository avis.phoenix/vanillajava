package org.avisphoenix.laboratory.final1;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class Monitor {
    private static int counter;
    private int id;
    private String marca;
    private double sizeInch;

    public static final double sizes[] = {10, 10.1, 13, 14, 15, 17, 19, 20, 23, 24, 27, 30, 32, 35, 40, 45,50, 55, 60, 65, 70, 80, 90 };

    public Monitor(){
        id=counter;
        counter++;
        marca = "Desconocida";
        sizeInch = 10;
    }

    public Monitor(String marca, double sizeInch){
        this.marca = marca;
        id=counter;
        counter++;
        this.sizeInch = sizeInch;
    }

    public Monitor(String json){
        id=counter;
        counter++;

        loadJSON(json);
    }

    @Override
    public String toString(){
        return "{ marca: " + marca + ", id: " + id + ", size: " + sizeInch + ", type: \"Monitor\"}" ;
    }

    public void loadJSON(String json){
        JsonObject myJSON = JsonParser.parseString(json).getAsJsonObject();
        
        marca = myJSON.get("marca").getAsString();
        sizeInch = myJSON.get("size").getAsDouble();
    }

    public String displayString(int indent){
        String indentSpace="";
        for (int i=0; i < indent; i++) indentSpace+="\t";
        return "Monitor #" + String.format("%4d",id) +":\n" +
                indentSpace + "Marca: " + marca + "\n" +
                indentSpace + "Tamaño: " + sizeInch + " pulg.";
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public double getSizeInch() {
        return sizeInch;
    }

    public void setSizeInch(double sizeInch) {
        this.sizeInch = sizeInch;
    }

    public static int globalCounter() { return counter; }

    
}

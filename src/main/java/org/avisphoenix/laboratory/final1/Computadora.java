package org.avisphoenix.laboratory.final1;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class Computadora {
    private int id;
    private static int counter;
    private String nombre;
    private Mouse raton;
    private Keyboard teclado;
    private Monitor monitor;

    public Computadora(){
        id=counter;
        counter++;
        nombre = "Sin asignar";
        raton = new Mouse("Desconcida", "Desconocido");
        teclado = new Keyboard("Desconcida", "Desconocido");
        monitor = new Monitor("Desconocida", 0);
    }

    public Computadora(String nombre, Mouse raton, Keyboard teclado, Monitor monitor){
        id=counter;
        counter++;

        this.nombre = nombre;
        this.raton = raton;
        this.teclado = teclado;
        this.monitor = monitor;
    }

    public Computadora(String json){
        id=counter;
        counter++;

        raton = null;
        teclado = null;
        monitor = null;

        loadJSON(json);
    }

    public String toString(){
        return "{ id: " + id + ", nombre: \"" + nombre + "\", Mouse: " + raton + ", Keyboard: " + teclado +
               ", Monitor: " + monitor + ", type: \"Computadora\" }";
    }

    public void loadJSON(String json){
        JsonObject myJSON = JsonParser.parseString(json).getAsJsonObject();
        nombre = myJSON.get("nombre").getAsString();

        if (raton == null){
            raton = new Mouse(myJSON.get("Mouse").toString());
        }else {
            raton.loadJSON(myJSON.get("Mouse").toString());
        }

        if (teclado == null){
            teclado = new Keyboard(myJSON.get("Keyboard").toString());
        }else {
            teclado.loadJSON(myJSON.get("Keyboard").toString());
        }

        if (monitor == null){
            monitor = new Monitor(myJSON.get("Monitor").toString());
        }else {
            monitor.loadJSON(myJSON.get("Monitor").toString());
        }

    }

    public String displayString(int indent){
        String indentSpace="";
        for (int i=0; i < indent; i++) indentSpace+="\t";
        return "Computadora #" + String.format("%4d",id) + ":\n"+
                indentSpace + "Nombre: " + nombre + "\n" +
                indentSpace +  raton.displayString(indent+1) + "\n" +
                indentSpace +  teclado.displayString(indent+1) + "\n" +
                indentSpace +  monitor.displayString(indent+1);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Mouse getRaton() {
        return raton;
    }

    public void setRaton(Mouse raton) {
        this.raton = raton;
    }

    public Keyboard getTeclado() {
        return teclado;
    }

    public void setTeclado(Keyboard teclado) {
        this.teclado = teclado;
    }

    public Monitor getMonitor() {
        return monitor;
    }

    public void setMonitor(Monitor monitor) {
        this.monitor = monitor;
    }

    public static int globalCounter() { return counter; }
}

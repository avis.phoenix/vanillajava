package org.avisphoenix.laboratory.final2.main;

import java.util.List;
import java.util.Scanner;

import org.avisphoenix.helloworld.PrintUtils;
import org.avisphoenix.laboratory.final2.business.CatalogoPeliculas;
import org.avisphoenix.laboratory.final2.business.CatalogoPeliculasVanilla;
import org.avisphoenix.laboratory.final2.domain.Pelicula;
import org.avisphoenix.laboratory.final2.excepciones.WritingErrorException;

public class LaboratorioFinal2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int opt = -1;
        String nombreArchivo = "";
        CatalogoPeliculas catalogoPeliculas = new CatalogoPeliculasVanilla();

        System.out.println("Catalogo de peliculas\nOpciones:");
        while (opt != 0) {
            while (nombreArchivo == "") {
                System.out.println("Escribe el nombre del archivo del catalogo:");
                nombreArchivo = scanner.nextLine();
            }
            System.out.println("\nOpciones:");
            System.out.println("1.- Iniciar catalogo de peliculas");
            System.out.println("2.- Agregar pelicula");
            System.out.println("3.- Listar catálogo");
            System.out.println("4.- Buscar película");
            System.out.println("0.- Salir");
            opt = PrintUtils.parseIntFromSystem("Elije una opción:", scanner);
            switch (opt) {
                case 0:
                    System.out.println("Adiós!");
                    break;
                case 1:
                    try{
                        catalogoPeliculas.initFile(nombreArchivo);
                        System.out.println("Archivo " + nombreArchivo + " inicializado.");
                    }catch(WritingErrorException e){
                        System.out.println(e.getMessage());
                    }
                    break;
                case 2:
                    agregarPelicula(catalogoPeliculas, nombreArchivo, scanner);
                    break;
                case 3:
                    listarPeliculas(catalogoPeliculas, nombreArchivo);
                    break;
                case 4:
                    buscarPelicula(catalogoPeliculas, nombreArchivo, scanner);
                    break;
                default:
                    System.out.println("Esa opción no existe, intenta otra.");
            }
            System.out.println("\n\n");
        }
    }

    private static void agregarPelicula(CatalogoPeliculas catalogoPeliculas, String filename, Scanner scanner){
        Pelicula movie = new Pelicula();
        movie.setName(PrintUtils.getStringFromSystem("Escribe el nombre de la pelicula", scanner));
        movie.setDirector(PrintUtils.getStringFromSystem("Escribe el nombre del director de la pelicula", scanner));
        movie.setYear(PrintUtils.parseIntFromSystem("Escribe el año en que salió la película", scanner));
        try {
            catalogoPeliculas.addMovie(movie, filename);
            System.out.println("Pelicula agregada exitosamente");
        }catch(WritingErrorException e){
            System.out.println(e.getMessage());
        }
    }

    private static void buscarPelicula(CatalogoPeliculas catalogoPeliculas, String filename, Scanner scanner){
        String partName = PrintUtils.getStringFromSystem("Escribe parte del nombre de la pelicula", scanner);
        try {
            List<Pelicula> resultados = catalogoPeliculas.findMovie(partName, filename);
            if (resultados!= null && !resultados.isEmpty()){
                System.out.println("Resultados (" + resultados.size() + "): ");
                System.out.println("Nombre\tYears");
                for (Pelicula pelicula : resultados) {
                    System.out.println(pelicula.getNombre() + "\t" + pelicula.getYear());
                }
            }else {
                System.out.println("No se encontró");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private static void listarPeliculas(CatalogoPeliculas catalogoPeliculas, String filename){
        try {
            List<Pelicula> resultados = catalogoPeliculas.getMovies(filename);
            if (resultados!= null && !resultados.isEmpty()){
                System.out.println("El archivo tiene " + resultados.size() + " peliculas.");
                System.out.println("Nombre\tYears\tDirector");
                for (Pelicula pelicula : resultados) {
                    System.out.println(pelicula.getNombre() + "\t" + pelicula.getYear() + "\t" + pelicula.getDirector());
                }
            } else {
                System.out.println("No se encontraron peliculas en ese arhcivo.");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }



}

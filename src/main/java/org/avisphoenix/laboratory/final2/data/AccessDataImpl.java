package org.avisphoenix.laboratory.final2.data;

import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import org.avisphoenix.laboratory.final2.domain.Pelicula;

import org.avisphoenix.laboratory.final2.excepciones.DeletingErrorException;
import org.avisphoenix.laboratory.final2.excepciones.OpenFileException;
import org.avisphoenix.laboratory.final2.excepciones.WritingErrorException;

public class AccessDataImpl implements AccessData {

    @Override
    public boolean exist(String filename) {
        return Files.exists(FileSystems.getDefault().getPath(filename));
    }

    @Override
    public List<Pelicula> readAll(String fileName) throws OpenFileException {
        List<Pelicula> output = new LinkedList<>();

        try (
            FileInputStream inputStream = new FileInputStream(fileName);
            Scanner sc = new Scanner(inputStream, StandardCharsets.UTF_8);
        ){
            while (sc.hasNextLine()) {
                String line = sc.nextLine();
                if (!line.replaceAll("\\s", "").isEmpty()){
                    output.add( new Pelicula(line));
                } 
            }
            // note that Scanner suppresses exceptions
            if (sc.ioException() != null) {
                throw sc.ioException();
            }
        } catch (IOException e) {
            throw new OpenFileException(fileName, e);
        }

        return output;
    }

    @Override
    public void writeItem(String filename, Pelicula item, boolean add) throws WritingErrorException {
        try (FileOutputStream file = new FileOutputStream(filename, add)) {
            file.write((item.toString() + "\n").getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            throw new WritingErrorException(filename, e);
        }
    }

    @Override
    public List<Pelicula> search(String fileName, String partialName, boolean matchCase) throws OpenFileException {
        
        List<Pelicula> output = new LinkedList<>();
        boolean found=false;
        Pelicula temp;

        try (
            FileInputStream inputStream = new FileInputStream(fileName);
            Scanner sc = new Scanner(inputStream, StandardCharsets.UTF_8)) {
            
            if (matchCase){
                while (sc.hasNextLine()) {
                    String line = sc.nextLine();
                    if (!line.replaceAll("\\s", "").isEmpty()){
                        temp =  new Pelicula(line);
                        found = temp.getNombre().contains(partialName);
                        if (found){
                            output.add(temp);
                        }
                    } 
                }
            } else {
                partialName = partialName.toUpperCase();
                while (sc.hasNextLine()) {
                    String line = sc.nextLine();
                    if (!line.replaceAll("\\s", "").isEmpty()){
                        temp =  new Pelicula(line);
                        found = temp.getNombre().toUpperCase().contains(partialName);
                        if (found){
                            output.add(temp);
                        }
                    } 
                }
            }
            // note that Scanner suppresses exceptions
            if (sc.ioException() != null) {
                throw sc.ioException();
            }
        } catch (IOException e) {
            throw new OpenFileException(fileName, e);
        }

        return output;
    }

    @Override
    public void createFile(String filename) throws WritingErrorException {
        try (FileOutputStream file = new FileOutputStream(filename)){
            
        } catch (IOException e) {
            throw new WritingErrorException(filename, e);
        }

    }

    @Override
    public void deleteFile(String filename) throws DeletingErrorException {
        try {
            Files.delete(FileSystems.getDefault().getPath(filename));
        } catch (Exception ex) {
            throw new DeletingErrorException(filename, ex);
        }
    }

}

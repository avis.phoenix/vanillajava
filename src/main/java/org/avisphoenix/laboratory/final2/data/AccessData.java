package org.avisphoenix.laboratory.final2.data;

import java.util.List;

import org.avisphoenix.laboratory.final2.domain.Pelicula;

import org.avisphoenix.laboratory.final2.excepciones.DeletingErrorException;
import org.avisphoenix.laboratory.final2.excepciones.OpenFileException;
import org.avisphoenix.laboratory.final2.excepciones.WritingErrorException;

public interface AccessData {
    public boolean exist(String file);

    public List<Pelicula> readAll(String file)  throws OpenFileException;

    public void writeItem(String file, Pelicula item, boolean add )  throws WritingErrorException;

    public List<Pelicula> search(String file, String partialName, boolean matchCase)  throws OpenFileException;

    public void createFile(String file) throws WritingErrorException ;

    public void deleteFile(String file)  throws DeletingErrorException ;
}

package org.avisphoenix.laboratory.final2.business;

import java.util.List;

import org.avisphoenix.laboratory.final2.domain.Pelicula;

import org.avisphoenix.laboratory.final2.excepciones.OpenFileException;
import org.avisphoenix.laboratory.final2.excepciones.WritingErrorException;

public interface CatalogoPeliculas {

    void addMovie(Pelicula movie, String filename) throws WritingErrorException;
    List<Pelicula> getMovies(String filename) throws OpenFileException;
    List<Pelicula> findMovie(String partName, String filename) throws OpenFileException;
    void initFile(String filename) throws WritingErrorException;

}

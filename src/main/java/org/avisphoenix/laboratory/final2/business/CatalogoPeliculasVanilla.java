package org.avisphoenix.laboratory.final2.business;

import java.util.LinkedList;
import java.util.List;

import org.avisphoenix.laboratory.final2.data.AccessData;
import org.avisphoenix.laboratory.final2.data.AccessDataImpl;
import org.avisphoenix.laboratory.final2.domain.Pelicula;
import org.avisphoenix.laboratory.final2.excepciones.OpenFileException;
import org.avisphoenix.laboratory.final2.excepciones.WritingErrorException;

public class CatalogoPeliculasVanilla implements CatalogoPeliculas {

    private AccessData accessData;

    public CatalogoPeliculasVanilla() {
        accessData = new AccessDataImpl();
    }

    @Override
    public void addMovie(Pelicula movie, String filename) throws WritingErrorException{
        accessData.writeItem(filename, movie, accessData.exist(filename));
    }

    @Override
    public List<Pelicula> getMovies(String filename) throws OpenFileException {
        return accessData.readAll(filename);
    }

    @Override
    public List<Pelicula> findMovie(String partName, String filename) throws OpenFileException {
        return accessData.search(filename, partName, false);
    }

    @Override
    public void initFile(String filename) throws WritingErrorException{

        accessData.createFile(filename);

        List<Pelicula> datos = new LinkedList<>();
        datos.add(new Pelicula("E.T.", "Stephen Spilberg", 1986));
        datos.add(new Pelicula("Jurassic Park", "Stephen Spilberg", 1991));
        datos.add(new Pelicula("The Lord of the Rings", "Peter Jackson", 2001));
        datos.add(new Pelicula("Eternal Sunshine of the Spotless Mind", "Michel Gondry", 2004));
        datos.add(new Pelicula("The Truman Show", "Peter Weir", 1998));

        for (Pelicula pelicula : datos) {
            accessData.writeItem(filename, pelicula, true);
        }
    }
    
}

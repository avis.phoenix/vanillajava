package org.avisphoenix.laboratory.final2.excepciones;

public class DeletingErrorException extends Exception{
    private final Exception realException;

    public DeletingErrorException(String fileName, Exception exception) {
        super("Error while deleting the file: " + fileName + ", Reason: " + exception.getMessage());
        this.realException = exception;
    }

    public DeletingErrorException(String fileName) {
        super("Error while deleting the file: " + fileName + ", Reason: Unknow");
        this.realException = null;
    }
    
    @Override
    public StackTraceElement[] getStackTrace() {
        if (realException != null){
            return realException.getStackTrace();
        } else {
            return super.getStackTrace();
        }
    }
    
}

package org.avisphoenix.laboratory.final2.excepciones;

public class WritingErrorException extends Exception {
    private final Exception realException;

    public WritingErrorException(String fileName, Exception exception) {
        super("Error while writting the file: " + fileName + ", Reason: " + exception.getMessage());
        this.realException = exception;
    }

    @Override
    public StackTraceElement[] getStackTrace() {
        return realException.getStackTrace();
    }
    
}

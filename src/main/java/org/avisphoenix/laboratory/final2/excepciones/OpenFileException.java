package org.avisphoenix.laboratory.final2.excepciones;

public class OpenFileException extends Exception{
    private final Exception realException;

    public OpenFileException(String fileName, Exception exception) {
        super("Error while opening the file: " + fileName + ", Reason: " + exception.getMessage());
        this.realException = exception;
    }
    
    @Override
    public StackTraceElement[] getStackTrace() {
        return realException.getStackTrace();
    }
    
}

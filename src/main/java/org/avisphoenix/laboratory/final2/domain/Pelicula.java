package org.avisphoenix.laboratory.final2.domain;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class Pelicula {
    private String name;
    private String director;
    private int year;

    public Pelicula(){
        name = "Unknow";
    }

    public Pelicula(String name, String director, int year){
        this.name = name;
        this.director = director;
        this.year = year;
    }

    public Pelicula(String json){
        loadJSON(json);
    }

    @Override
    public String toString() {
        return "{ type: \"Pelicula\", name: \"" + name + "\", director: \"" + director + "\", year: " + year + " }";
    }

    public void loadJSON(String json){
        JsonObject myJSON = JsonParser.parseString(json).getAsJsonObject();
        name = myJSON.get("name").getAsString();
        year = myJSON.get("year").getAsInt();
        director = myJSON.get("director").getAsString();
    }

    public String getNombre() {
        return name;
    }

    public void setNombre(String nombre) {
        this.name = nombre;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    
}

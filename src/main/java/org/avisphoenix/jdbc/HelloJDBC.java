package org.avisphoenix.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Logger;

import org.avisphoenix.helloworld.PrintUtils;
import org.avisphoenix.jdbc.dao.PersonaDAO;
import org.avisphoenix.jdbc.domain.Persona;
import org.avisphoenix.jdbc.util.ConnectionManager;

public class HelloJDBC {
    public static void main(String[] args) {
        usePersonasTest();
    }

    private static void usePersonasTest(){
        String urlServer = "jdbc:mysql://localhost:3306/java?useSSL=false&useTimezone=true&serverTimezone=UTC&allowPublicKeyRetrieval=true";
        ConnectionManager connection = new ConnectionManager(urlServer, "dev", "pCjZu!o2F6FO");
        PersonaDAO personaDAO = new PersonaDAO(connection);

        Scanner scanner = new Scanner(System.in);
        int opt = -1;
        int numTemp;
        Persona persona = null;
        final String registros = " registros.";

        System.out.println("Manejo de base de datos - Personas\nOpciones:");
        while (opt != 0) {
            System.out.println("\nOpciones:");
            System.out.println("1.- Ver base de datos");
            System.out.println("2.- Agregar persona");
            System.out.println("3.- Eliminar persona");
            System.out.println("4.- Modificar persona");
            System.out.println("0.- Salir");
            opt = PrintUtils.parseIntFromSystem("Elije una opción:", scanner);
            switch (opt) {
                case 0:
                    System.out.println("Adiós!");
                    break;
                case 1:
                    try{
                        List<Persona> resultado = personaDAO.getPersonas();
                        for (Persona item: resultado){
                            System.out.println(item);
                        }
                    } catch (Exception ex){
                        System.out.println("ERROR: trying to get data from personaDAO. Reason:" + ex.getMessage());
                    }
                    break;
                case 2:
                    persona = new Persona();
                    persona.setNombre(PrintUtils.getStringFromSystem("Escribe el nombre de la persona: ", scanner));
                    persona.setApellido(PrintUtils.getStringFromSystem("Escribe el apellido de la persona: ", scanner));
                    persona.setEmail(PrintUtils.getStringFromSystem("Escribe el correo de la persona: ", scanner));
                    persona.setTelefono(PrintUtils.getStringFromSystem("Escribe el telefono de la persona: ", scanner));
                    try{
                        numTemp = personaDAO.insert(persona);
                        System.out.println("Se insertaron: " + numTemp + registros);
                    } catch (Exception ex){
                        System.out.println("ERROR: trying to insert data from personaDAO. Reason:" + ex.getMessage());
                    }
                    break;
                case 3:
                    numTemp = PrintUtils.parseIntFromSystem("Escribe el id de la persona:", scanner);
                    try{
                        numTemp = personaDAO.delete(new Persona(numTemp));
                        System.out.println("Se eliminaron: " + numTemp + registros);
                    } catch (Exception ex){
                        System.out.println("ERROR: trying to delete data from personaDAO. Reason:" + ex.getMessage());
                    }
                    break;
                case 4:
                    persona = new Persona();
                    persona.setId(PrintUtils.parseIntFromSystem("Escribe el ID de la persona: ", scanner));
                    persona.setNombre(PrintUtils.getStringFromSystem("Escribe el nombre de la persona: ", scanner));
                    persona.setApellido(PrintUtils.getStringFromSystem("Escribe el apellido de la persona: ", scanner));
                    persona.setEmail(PrintUtils.getStringFromSystem("Escribe el correo de la persona: ", scanner));
                    persona.setTelefono(PrintUtils.getStringFromSystem("Escribe el telefono de la persona: ", scanner));
                    try{
                        numTemp = personaDAO.update(persona);
                        System.out.println("Se modificaron: " + numTemp + registros);
                    } catch (Exception ex){
                        System.out.println("ERROR: trying to update data from personaDAO. Reason:" + ex.getMessage());
                    }
                    break;
                default:
                    System.out.println("Esa opción no existe, intenta otra.");
            }
            System.out.println("\n\n");
        }

        
    }

    private static void basicUseJDBC(){
        String urlServer = "jdbc:mysql://localhost:3306/java?useSSL=false&useTimezone=true&serverTimezone=UTC&allowPublicKeyRetrieval=true";
        // Class.forName("com.mysql.jdbc.Driver"); for lower versions of java 6
        try (
            Connection connection = DriverManager.getConnection(urlServer, "dev", "pCjZu!o2F6FO");
            Statement statement = connection.createStatement();
            ){
            var sqlStatement = "SELECT id, nombre, apellido, email, telefono FROM personas";
            ResultSet result = statement.executeQuery(sqlStatement);
            System.out.println("Resultados");
            while(result.next()){
                System.out.println(String.valueOf(result.getInt("id")) + "\t" + result.getString("nombre") + "\t" 
                                   + result.getString("apellido") + "\t" + result.getString("email")  + "\t" + result.getString("telefono") );
                
            }
            result.close();
        } catch (SQLException e) {
            //Logger.getLogger("SQL Connection Error: " + e.getMessage());
            System.out.println("SQL Connection Error: " + e.getMessage());
        }
    }
}

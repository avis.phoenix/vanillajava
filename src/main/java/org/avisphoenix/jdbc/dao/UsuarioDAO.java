package org.avisphoenix.jdbc.dao;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.avisphoenix.jdbc.domain.Usuario;
import org.avisphoenix.jdbc.util.ConnectionManager;
import org.avisphoenix.jdbc.util.ParametersPreparedStatement;
import org.avisphoenix.jdbc.util.typeDataResults;

public class UsuarioDAO {
    private final ConnectionManager connection;
    private static final String SQL_SELECT = "SELECT id, username, password FROM usuarios;";
    private static final String SQL_INSERT = "INSERT INTO usuarios( username, password ) VALUES( ?, ? );";
    private static final String SQL_UPDATE = "UPDATE usuarios SET username=?, password=? WHERE id=?;";
    private static final String SQL_DELETE = "DELETE FROM usuarios WHERE id=?;";

    public UsuarioDAO(ConnectionManager connection) {
        this.connection = connection;
    }

    public List<Usuario> getUsuarios() throws SQLException{
        List<Usuario> output = new LinkedList<>();

        Map<String, typeDataResults> propertiesList = new HashMap<>();
        propertiesList.put("id", typeDataResults.INTEGER);
        propertiesList.put("username", typeDataResults.STRING);
        propertiesList.put("password", typeDataResults.STRING);

        List<Map<String, Object>> results = connection.getResultsQuery(SQL_SELECT, propertiesList);
        Usuario Usuario = null;

        for(Map<String, Object> item: results){
            Usuario = new Usuario(((Integer)item.get("id")).intValue(),
                                   (String)item.get("username"),
                                   (String)item.get("password"));
            output.add(Usuario);
        }

        return output;
    }

    public int insert(Usuario user) throws SQLException{
        int rowCount;
        List<ParametersPreparedStatement> params = new LinkedList<>();

        params.add( new ParametersPreparedStatement( user.getUsername(), typeDataResults.STRING ) );
        params.add( new ParametersPreparedStatement( user.getPassword(), typeDataResults.STRING ) );

        rowCount = connection.getResultsUpdate(SQL_INSERT, params);

        return rowCount;
    }

    public int update(Usuario p) throws SQLException{
        int rowCount;
        List<ParametersPreparedStatement> params = new LinkedList<>();

        params.add( new ParametersPreparedStatement( p.getUsername(), typeDataResults.STRING ) );
        params.add( new ParametersPreparedStatement( p.getPassword(), typeDataResults.STRING ) );
        params.add( new ParametersPreparedStatement( p.getId(), typeDataResults.INTEGER ) );

        rowCount = connection.getResultsUpdate(SQL_UPDATE, params);

        return rowCount;
    }

    public int delete(Usuario p) throws SQLException{
        int rowCount;
        List<ParametersPreparedStatement> params = new LinkedList<>();
        
        params.add( new ParametersPreparedStatement( p.getId(), typeDataResults.INTEGER ) );

        rowCount = connection.getResultsUpdate(SQL_DELETE, params);

        return rowCount;
    }
}

package org.avisphoenix.jdbc.dao;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.avisphoenix.jdbc.domain.Persona;
import org.avisphoenix.jdbc.util.ConnectionManager;
import org.avisphoenix.jdbc.util.ParametersPreparedStatement;
import org.avisphoenix.jdbc.util.typeDataResults;

public class PersonaDAO {
    private final ConnectionManager connection;
    private static final String SQL_SELECT = "SELECT id, nombre, apellido, email, telefono FROM personas;";
    private static final String SQL_INSERT = "INSERT INTO personas( nombre, apellido, email, telefono ) VALUES( ?, ?, ?, ? );";
    private static final String SQL_UPDATE = "UPDATE personas SET nombre=?, apellido=?, email=?, telefono=? WHERE id=?;";
    private static final String SQL_DELETE = "DELETE FROM personas WHERE id=?;";

    public PersonaDAO(ConnectionManager connection) {
        this.connection = connection;
    }

    public List<Persona> getPersonas() throws SQLException{
        List<Persona> output = new LinkedList<>();

        Map<String, typeDataResults> propertiesList = new HashMap<>();
        propertiesList.put("id", typeDataResults.INTEGER);
        propertiesList.put("nombre", typeDataResults.STRING);
        propertiesList.put("apellido", typeDataResults.STRING);
        propertiesList.put("email", typeDataResults.STRING);
        propertiesList.put("telefono", typeDataResults.STRING);

        List<Map<String, Object>> results = connection.getResultsQuery(SQL_SELECT, propertiesList);
        Persona persona = null;

        for(Map<String, Object> item: results){
            persona = new Persona(((Integer)item.get("id")).intValue(),
                                   (String)item.get("nombre"),
                                   (String)item.get("apellido"),
                                   (String)item.get("email"),
                                   (String)item.get("telefono"));
            output.add(persona);
        }

        return output;
    }

    public int insert(Persona p) throws SQLException{
        int rowCount;
        List<ParametersPreparedStatement> params = new LinkedList<>();

        params.add( new ParametersPreparedStatement( p.getNombre(), typeDataResults.STRING ) );
        params.add( new ParametersPreparedStatement( p.getApellido(), typeDataResults.STRING ) );
        params.add( new ParametersPreparedStatement( p.getEmail(), typeDataResults.STRING ) );
        params.add( new ParametersPreparedStatement( p.getTelefono(), typeDataResults.STRING ) );

        rowCount = connection.getResultsUpdate(SQL_INSERT, params);

        return rowCount;
    }

    public int update(Persona p) throws SQLException{
        int rowCount;
        List<ParametersPreparedStatement> params = new LinkedList<>();

        params.add( new ParametersPreparedStatement( p.getNombre(), typeDataResults.STRING ) );
        params.add( new ParametersPreparedStatement( p.getApellido(), typeDataResults.STRING ) );
        params.add( new ParametersPreparedStatement( p.getEmail(), typeDataResults.STRING ) );
        params.add( new ParametersPreparedStatement( p.getTelefono(), typeDataResults.STRING ) );
        params.add( new ParametersPreparedStatement( p.getId(), typeDataResults.INTEGER ) );

        rowCount = connection.getResultsUpdate(SQL_UPDATE, params);

        return rowCount;
    }

    public int delete(Persona p) throws SQLException{
        int rowCount;
        List<ParametersPreparedStatement> params = new LinkedList<>();
        
        params.add( new ParametersPreparedStatement( p.getId(), typeDataResults.INTEGER ) );

        rowCount = connection.getResultsUpdate(SQL_DELETE, params);

        return rowCount;
    }

}

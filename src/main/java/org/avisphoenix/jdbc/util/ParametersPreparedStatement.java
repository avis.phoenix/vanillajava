package org.avisphoenix.jdbc.util;

public class ParametersPreparedStatement {
    private Object value;
    private typeDataResults type;

    public ParametersPreparedStatement(Object value, typeDataResults type) {
        this.value = value;
        this.type = type;
    }

    public ParametersPreparedStatement() {
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public typeDataResults getType() {
        return type;
    }

    public void setType(typeDataResults type) {
        this.type = type;
    }

}

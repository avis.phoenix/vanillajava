package org.avisphoenix.jdbc.util;

import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class ConnectionManager {
    private final String urlServer;
    private final String user;
    private final String password;

    public ConnectionManager(String urlServer, String user, String password) {
        this.urlServer = urlServer;
        this.user = user;
        this.password = password;
    }

    public List<Map<String, Object>> getResultsQuery(String sqlQuery, Map<String, typeDataResults> propertiesList)
            throws SQLException {
        List<Map<String, Object>> output = null;

        try (Connection connection = DriverManager.getConnection(urlServer, user, password);
                PreparedStatement statement = connection.prepareStatement(sqlQuery);
                ResultSet result = statement.executeQuery();) {

            output = new LinkedList<>();

            while (result.next()) {
                Map<String, Object> objMap = new HashMap<>();
                for (Entry<String, typeDataResults> entry : propertiesList.entrySet()) {
                    switch (entry.getValue()) {
                        case STRING: objMap.put(entry.getKey(), result.getString(entry.getKey())); break;
                        case INTEGER: objMap.put(entry.getKey(), Integer.valueOf(result.getInt(entry.getKey()))); break;
                        case DOUBLE: objMap.put(entry.getKey(), Double.valueOf(result.getDouble(entry.getKey()))); break;
                        case BOOLEAN: objMap.put(entry.getKey(), Boolean.valueOf(result.getBoolean(entry.getKey()))); break;
                        case DATE: objMap.put(entry.getKey(), result.getDate(entry.getKey())); break;
                        case ARRAY: objMap.put(entry.getKey(), result.getArray(entry.getKey())); break;
                        case BLOB: objMap.put(entry.getKey(), result.getBlob(entry.getKey())); break;
                        case CLOB: objMap.put(entry.getKey(), result.getClob(entry.getKey())); break;
                    }
                }
                output.add(objMap);
            }
        } catch (SQLException ex) {
            // Logger.getError(ex.getMessage());
            throw ex;
        }

        return output;
    }

    public int getResultsUpdate(String sqlQuery, List<ParametersPreparedStatement> parameters) throws SQLException {
        int output = -1;
        int i;
        try (Connection connection = DriverManager.getConnection(urlServer, user, password);
                PreparedStatement statement = connection.prepareStatement(sqlQuery);) {
            i = 1;
            for (ParametersPreparedStatement param : parameters) {
                switch (param.getType()) {
                    case STRING: statement.setString(i, (String) param.getValue()); break;
                    case INTEGER: statement.setInt(i, (Integer) param.getValue()); break;
                    case DOUBLE: statement.setDouble(i, (Double) param.getValue()); break;
                    case BOOLEAN: statement.setBoolean(i, (Boolean) param.getValue()); break;
                    case DATE: statement.setDate(i, (Date) param.getValue()); break;
                    case ARRAY: statement.setArray(i, (Array) param.getValue()); break;
                    case BLOB: statement.setBlob(i, (Blob) param.getValue()); break;
                    case CLOB: statement.setClob(i, (Clob) param.getValue()); break;
                }
                i++;
            }
            output = statement.executeUpdate();
        } catch (SQLException ex) {
            // Logger.getError(ex.getMessage());
            throw ex;
        }

        return output;
    }

}

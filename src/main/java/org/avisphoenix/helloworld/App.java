package org.avisphoenix.helloworld;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import org.avisphoenix.laboratory.final1.Computadora;
import org.avisphoenix.laboratory.final1.DispositivoEntrada;
import org.avisphoenix.laboratory.final1.Keyboard;
import org.avisphoenix.laboratory.final1.Mouse;
import org.avisphoenix.laboratory.final1.Orden;
import org.avisphoenix.laboratory.final1.Monitor;

/**
 * Practice exercise
 *
 */
public class App {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean stillRun = true;
        List<Orden> ordenes = null;
        int opt;

        System.out.println("Generador de ordenes masivo\nOpciones:");
        while (stillRun) {
            System.out.println("Cantidad de ordenes actuales: " + (ordenes != null ? ordenes.size() : 0));
            System.out.println("Cantidad de Computadoras actuales: " + Computadora.globalCounter());
            System.out.println("\nOpciones");
            System.out.println("1.- Generar ordenes aleatoriamente");
            System.out.println("2.- Mostrar un orden aleatoria");
            System.out.println("3.- Mostrar un orden especifica");
            System.out.println("4.- Mostrar todas las ordenes en pantalla");
            System.out.println("5.- Guardar ordenes en un archivo");
            System.out.println("6.- Cargar ordenes de un archivo");
            System.out.println("0.- Salir");
            opt = PrintUtils.parseIntFromSystem("Elije una opción:", scanner);
            switch (opt) {
                case 0:
                    stillRun = false;
                    System.out.println("Adiós!");
                    break;
                case 1:
                    opt = PrintUtils.parseIntFromSystem("De qué tamaño quieres la lista de ordenes?: ", scanner);
                    if (opt > 0) {
                        System.out.println("Generando...");
                        if (ordenes != null)
                            ordenes.clear();
                        ordenes = generateOrdenesList(opt);
                        System.out.println("¡Listo!");
                    } else {
                        System.out.println("Debes elegir un tamaño positivo.");
                    }
                    break;
                case 2:
                    if (ordenes != null && ordenes.size() > 0) {
                        opt = (new Random()).nextInt(ordenes.size());
                        displayOrden(ordenes, opt);
                    } else {
                        System.out.println("No hay ordenes aún.");
                    }
                    break;
                case 3:
                    if (ordenes != null && ordenes.size() > 0) {
                        opt = PrintUtils.parseIntFromSystem("¿Qué orden quieres ver? (0-" + (ordenes.size() - 1) + ")",
                                scanner);
                        displayOrden(ordenes, opt);
                    } else {
                        System.out.println("No hay ordenes aún.");
                    }
                    break;
                case 4:
                    if (ordenes != null && ordenes.size() > 0) {
                        displayAllOrdenes(ordenes);
                    } else {
                        System.out.println("No hay ordenes aún.");
                    }
                    break;
                case 5:
                    if (ordenes != null && ordenes.size() > 0){
                        System.out.println("Escribe el nombre del archivo (escribe un único caracter para cancelar): ");
                        String nombre = scanner.nextLine();
                        if (nombre.length() >= 2){
                            System.out.println("Guardando...");
                            saveJSON(nombre+".jlist", ordenes);
                        }else {
                            System.out.println("Cancelado.");
                        }
                    }else {
                        System.out.println("No hay ordenes aún.");
                    }
                    break;
                case 6:
                    System.out.println("Escribe el nombre del archivo (escribe un único caracter para cancelar): ");
                    String nombre = scanner.nextLine();
                    if (nombre.length() >= 2){
                        System.out.println("Cargando...");
                        if (ordenes != null && ordenes.size() > 0){
                            ordenes.clear();
                        }
                        ordenes = loadJSON(nombre+".jlist");
                    }else {
                        System.out.println("Cancelado.");
                    }
                    break;
                default:
                    System.out.println("Esa opción no existe, intenta otra.");
            }
            System.out.println("\n\n\n\n\n\n");
        }

    }

    private static List<Orden> generateOrdenesList(int listSize) {
        List<Orden> ordenes = new LinkedList<>();
        Orden ordenTemp;
        int i, j;
        Random random = new Random();
        final int sizeMarcasDE = DispositivoEntrada.Marcas.length;
        final int sizeTiposDE = DispositivoEntrada.DipositivosTipos.length;
        final int sizeMonitores = Monitor.sizes.length;

        for (i = 0; i < listSize; i++) {
            ordenTemp = new Orden();
            for (j = 0; j < random.nextInt(Orden.MAX_COMPUTADORAS); j++) {
                ordenTemp.agregarComputadora(new Computadora(PrintUtils.randomString(random.nextInt(3) + 4),
                        new Mouse(DispositivoEntrada.Marcas[random.nextInt(sizeMarcasDE)],
                                DispositivoEntrada.DipositivosTipos[random.nextInt(sizeTiposDE)]),
                        new Keyboard(DispositivoEntrada.Marcas[random.nextInt(sizeMarcasDE)],
                                DispositivoEntrada.DipositivosTipos[random.nextInt(sizeTiposDE)]),
                        new Monitor(DispositivoEntrada.Marcas[random.nextInt(sizeMarcasDE)],
                                Monitor.sizes[random.nextInt(sizeMonitores)])));
            }
            ordenes.add(ordenTemp);
        }

        return ordenes;
    }

    private static void displayOrden(List<Orden> ordenes, int i) {
        System.out.println("");
        if (i >= 0 && i < ordenes.size())
            ordenes.get(i).mostrarOrden();
    }

    private static void displayAllOrdenes(List<Orden> ordenes) {
        System.out.println("");
        ordenes.forEach((Orden orden) -> {
            orden.mostrarOrden();
        });
    }

    private static void saveJSON(String fileName, List<Orden> ordenes) {
        FileOutputStream file = null;
        try {
            file = new FileOutputStream(fileName);
            for (Orden orden : ordenes) {
                file.write((orden.toStringJSON() + "\n").getBytes("UTF-8"));
            }
        } catch (IOException e) {
            System.out.println("ERROR: No se puede crear el archivo, mensaje: " + e.getMessage());
        } finally{
            if (file != null){
                try {
                    file.close();
                } catch (IOException e) {
                    System.out.println("ERROR: No se puede cerrar el archivo, mensaje: " + e.getMessage());
                }
            }
        }
    }

    private static List<Orden> loadJSON(String fileName){
        List<Orden> ordenes = new LinkedList<>();
        FileInputStream inputStream = null;
        Scanner sc = null;

        try {
            inputStream = new FileInputStream(fileName);
            sc = new Scanner(inputStream, "UTF-8");
            while (sc.hasNextLine()) {
                String line = sc.nextLine();
                if (!line.replaceAll(" ", "").isEmpty()){
                    ordenes.add( new Orden(line));
                } 
            }
            // note that Scanner suppresses exceptions
            if (sc.ioException() != null) {
                throw sc.ioException();
            }
        } catch (IOException e) {
            System.out.println("ERROR: No se puede abrir el archivo, mensaje: " + e.getMessage());
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    System.out.println("ERROR: No se puede cerrar el archivo, mensaje: " + e.getMessage());
                }
            }
            if (sc != null) {
                sc.close();
            }
        }

        return ordenes;
    }
    
}

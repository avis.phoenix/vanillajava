package org.avisphoenix.helloworld;

import java.util.Random;
import java.util.Scanner;

public class PrintUtils {

    public static String getStringFromSystem(String text, Scanner scanner){
        String output;
        
        System.out.println( text );
        output = scanner.nextLine();

        return output;
    }

    public static double parseDoubleFromSystem(String text, Scanner scanner){
        String temp;
        double output=0;
        boolean allGood;
        do{
            allGood = true;
            System.out.println( text );
            temp = scanner.nextLine();
            try {
                output = Double.parseDouble(temp);
            } catch(Exception ex){
                allGood = false;
                System.out.println( "Error: \"" + temp + "\" no es un número. Escribe un número por favor." );
            }
        } while (!allGood);

        return output;
    }

    public static int parseIntFromSystem(String text, Scanner scanner){
        String temp;
        int output=0;
        boolean allGood;
        do{
            allGood = true;
            System.out.println( text );
            temp = scanner.nextLine();
            try {
                output = Integer.parseInt(temp);
            } catch(Exception ex){
                allGood = false;
                System.out.println( "Error: \"" + temp + "\" no es un entero. Escribe un entero por favor." );
            }
        } while (!allGood);

        return output;
    }

    public static boolean parseBooleanFromSystem(String text, Scanner scanner){
        String temp;
        boolean output=false;
        boolean allGood;
        do{
            allGood = true;
            System.out.println( text );
            temp = scanner.nextLine();
            temp = temp.toLowerCase();
            if ("true".equals(temp) || "false".equals(temp)) {
                output = Boolean.parseBoolean(temp);
            } else{
                allGood = false;
                System.out.println( "Error: \"" + temp + "\" no es true o false. Escribe \"true\" o \"false\" por favor." );
            }
        } while (!allGood);

        return output;
    }

    public static String randomString(int length){
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        Random random = new Random();

        return random.ints(leftLimit, rightLimit + 1)
        .limit(length)
        .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
        .toString();
    }
}

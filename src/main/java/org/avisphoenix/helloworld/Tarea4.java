package org.avisphoenix.helloworld;

import java.util.Scanner;

/**
 * Practie exercise
 *
 */
public class Tarea4 
{
    public static void main( String[] args )
    {
        Scanner scanner = new Scanner(System.in);
        String bookTitle;
        boolean bookFreeShip;
        int bookIdValue;
        double bookPriceValue;

        System.out.println( "Registro de un libro" );
        System.out.println( "Proporciona el nombre: " );
        bookTitle = scanner.nextLine();
        bookIdValue = parseIntFromSystem("Proporciona el id: ", scanner);
        bookPriceValue = parseDoubleFromSystem("Proporciona el precio: " , scanner);
        bookFreeShip = parseBooleanFromSystem("Proporciona el envío gratuito: (true/false)", scanner);
        System.out.println( "Libro registrado: " );
        System.out.println( "\t" + bookTitle + " #" +bookIdValue);
        System.out.println( "\tPrecio: $" + String.format("%.2f", bookPriceValue));
        System.out.println( "\tEnvio Gratuito: " + (bookFreeShip ? "true" : "false"));

    }

    private static int parseIntFromSystem(String text, Scanner scanner){
        String temp;
        int output=0;
        boolean allGood;
        do{
            allGood = true;
            System.out.println( text );
            temp = scanner.nextLine();
            try {
                output = Integer.parseInt(temp);
            } catch(Exception ex){
                allGood = false;
                System.out.println( "Error: \"" + temp + "\" no es un entero. Escribe un entero por favor." );
            }
        } while (!allGood);

        return output;
    }

    private static double parseDoubleFromSystem(String text, Scanner scanner){
        String temp;
        double output=0;
        boolean allGood;
        do{
            allGood = true;
            System.out.println( text );
            temp = scanner.nextLine();
            try {
                output = Double.parseDouble(temp);
            } catch(Exception ex){
                allGood = false;
                System.out.println( "Error: \"" + temp + "\" no es un número. Escribe un número por favor." );
            }
        } while (!allGood);

        return output;
    }

    private static boolean parseBooleanFromSystem(String text, Scanner scanner){
        String temp;
        boolean output=false;
        boolean allGood;
        do{
            allGood = true;
            System.out.println( text );
            temp = scanner.nextLine();
            temp = temp.toLowerCase();
            if ("true".equals(temp) || "false".equals(temp)) {
                output = Boolean.parseBoolean(temp);
            } else{
                allGood = false;
                System.out.println( "Error: \"" + temp + "\" no es true o false. Escribe \"true\" o \"false\" por favor." );
            }
        } while (!allGood);

        return output;
    }
}

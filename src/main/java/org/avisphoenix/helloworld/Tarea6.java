package org.avisphoenix.helloworld;

import java.util.Scanner;

/**
 * Practice exercise
 *
 */
public class Tarea6 
{
    public static void main( String[] args )
    {
        Scanner scanner = new Scanner(System.in);
        double calif;
        String score="Valor desconocido";

        calif = parseDoubleFromSystem("Escribe la calificacion (entre 0 y 10): ", scanner);
        if (calif >= 0 && calif < 6) score="F";
        else if (calif >= 6 && calif < 7) score="D";
        else if (calif >= 7 && calif < 8) score="C";
        else if (calif >= 8 && calif < 9) score="B";
        else if (calif >= 9 && calif <= 10) score="A";

        System.out.println(score);
    }

    private static double parseDoubleFromSystem(String text, Scanner scanner){
        String temp;
        double output=0;
        boolean allGood;
        do{
            allGood = true;
            System.out.println( text );
            temp = scanner.nextLine();
            try {
                output = Double.parseDouble(temp);
            } catch(Exception ex){
                allGood = false;
                System.out.println( "Error: \"" + temp + "\" no es un número. Escribe un número por favor." );
            }
        } while (!allGood);

        return output;
    }
}
